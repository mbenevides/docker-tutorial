BANDIT_FLAGS := -r -ll -ii
PYVERSION := py38
SHELL := bash
SOURCE := app/ tests/

DEFAULT_TARGET := development

build: ## Builds all required images
	@COMPOSE_DOCKER_CLI_BUILD=1 DOCKER_BUILDKIT=1 \
		docker-compose build --force-rm

up: ## Spawns the required containers for a working API
	@COMPOSE_DOCKER_CLI_BUILD=1 DOCKER_BUILDKIT=1 \
		docker-compose up --force-recreate --build

