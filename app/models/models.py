from tortoise import fields, models


class Item(models.Model):
    value = fields.TextField()
    created_at = fields.DatetimeField(auto_now_add=True)

    def __str__(self):
        return self.value
