#!/usr/bin/env bash

echo "Resolving Hostname for Postgres..."

if [[ $APP_SETTINGS == "production" ]]; then
    DB_URL=${DATABASE_URL}
else
    DB_URL=${DATABASE_TEST_URL}
fi

REGEX="(?<=@).+(?=:)"
HOST="$(echo "${DB_URL}" | grep -oP "${REGEX}")"

echo "Waiting for Postgres in $(echo "${HOST}" | head -c 10)..."

until nc -vz "${HOST}" 5432; do
  >&2 echo "Postgres is unavailable - sleeping"
  sleep 1
done

echo "Postgres started"

source $HOME/.poetry/env \
    && poetry run uvicorn main:app --reload --workers 1 --host 0.0.0.0 --port 8000
