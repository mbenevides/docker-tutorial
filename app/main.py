from fastapi import FastAPI
from loguru import logger as log
from tortoise.contrib.fastapi import register_tortoise

from api import item, healthcheck
from db import init_db


app = FastAPI()


def create_application() -> FastAPI:
    application = FastAPI()

    application.include_router(item.router, prefix="/v1")
    application.include_router(healthcheck.router)

    return application


app = create_application()


@app.on_event("startup")
async def startup_event():
    log.info("Starting up...")
    init_db(app)


@app.on_event("shutdown")
async def shutdown_event():
    log.info("Shutting down...")
