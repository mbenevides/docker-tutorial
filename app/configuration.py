import os
from functools import lru_cache
from pathlib import Path

from dotenv import load_dotenv
from loguru import logger as log
from pydantic import BaseSettings

PROJECT_ROOT = Path(__file__).parent.parent.absolute()
API_ROOT = PROJECT_ROOT / Path("app")

class Settings(BaseSettings):
    db_url: str = os.getenv(
        "DATABASE_URL",
        "postgres://postgres:postgres@db:5432/web_dev"
    )
    environment: str = os.getenv("ENVIRONMENT", "development")
    testing: bool = os.getenv("TESTING", 0)


@lru_cache()
def get_settings() -> BaseSettings:
    log.info("Loading config settings from the environment...")
    return Settings()
