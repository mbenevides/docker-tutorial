from fastapi import FastAPI
from loguru import logger as log
from tortoise import Tortoise
from tortoise.contrib.fastapi import register_tortoise

from configuration import get_settings

def init_db(app: FastAPI) -> None:
    register_tortoise(
        app,
        db_url=get_settings().db_url,
        modules={"models": ["tortoise"]},
        generate_schemas=False,
        add_exception_handlers=True,
    )


async def generate_schema() -> None:
    log.info("Initializing Tortoise...")

    await Tortoise.init(
        db_url=get_settings().db_url,
        modules={"models": ["tortoise"]},
    )
    log.info("Generating database schema via Tortoise...")
    await Tortoise.generate_schemas()
    await Tortoise.close_connections()


if __name__ == '__main__':
    run_async(generate_schema())
