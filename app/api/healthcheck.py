from fastapi import APIRouter, Depends

from configuration import get_settings, Settings


router = APIRouter()


@router.get("/status")
async def status(settings: Settings = Depends(get_settings)):
    return {
        "environment": settings.environment,
        "testing": settings.testing
    }

